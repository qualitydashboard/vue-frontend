'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API_URL: "https://qualitydashboard-staging.herokuapp.com/api"
}
