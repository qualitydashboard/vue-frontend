import jwt_decode from 'jwt-decode'
import utils from './../plugins/utils'

export default class User {
  constructor(username, email, password) {
    this.id = null
    this.username = username;
    this.email = email;
    this.password = password;

    this.token = null
  }

  /**
   *
   * @param delta ms
   * @returns {boolean}
   */
  hasAccessExpired(delta=5000){
    return (new Date()).getTime()+delta > jwt_decode(this.token.access).exp
  }


  getJWT(){
    // detect is token is outdated
    if( this.hasAccessExpired()){
      // outdated, need update
      return "nop"
    }
    // update if needed

    return this.token.access

  }

  exportJSON(){
    return {
      username: this.username,
      email: this.email,
      id: this.id,
      token: this.token
    }
  }

  save(){
    localStorage.setItem('user',JSON.stringify(this.exportJSON()))
  }

  setToken(token){
    this.token = {
      access: token.access,
      refresh: token.refresh
    }

    this.id = jwt_decode(token.access).user_id
  }

  static load(json_user){
    var user = new User(json_user.username, json_user.email, '');
    user.setToken(json_user.token)

    user.id = json_user.id;
    const refresh_jwt_expiration = jwt_decode(user.token.refresh).exp
    console.log(refresh_jwt_expiration);
    console.log(utils.currentTimestamp);
    if (utils.currentTimestamp > refresh_jwt_expiration*1000){
      // refresh token is outdated ! , need conventional login
      return null
    }
    return user
  }


  static getCurrent(){
    const json_user = JSON.parse(localStorage.getItem('user'))
    return json_user ? User.load(json_user) : null
  }


}
