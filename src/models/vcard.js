export default class Vcard {


    constructor() {
        this.surname = null;
        this.name = null;
        this.position = null;

        this.company = null;
        this.phone = null;
        this.phone_private = null;
        this.phone_pro = null;
        this.mail = null;
        this.mail_pro = null;
        this.website = null;
        this.address = null;
    }

    print(){
        let content = "";
        content += `BEGIN:VCARD\r\nVERSION:3.0\r\n`;
        content += `N:${this.name};${this.surname}\r\n`;
        content += `FN:${this.surname} ${this.name}\r\n`;
        content += `TEL;TYPE=work,voice:${this.phone_pro}\r\n`;
        if (this.address) {
            content += `ADR;TYPE=HOME:;;${this.address.address};${this.address.city};${this.address.zipCode};${this.address.country}\r\n`;
        }


        content += `TEL;TYPE=private,voice:${this.phone_private}\r\n`;
        content += `EMAIL;TYPE=internet,pref:${this.mail}\r\n`;
        content += `ORG:${this.company}\r\n`;
        content += 'REV:20111013T195243Z\r\n';
        content += 'END:VCARD';

        return content;
    }

}
