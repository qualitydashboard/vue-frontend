//import About from "../../components/About";
//import HelloWorld from "../../components/HelloWorld";
import routes_documents from './documents'
import DocumentApp from './../../components/pages/documents/App'
import guards from "../guards"
import Login from "../../components/pages/account/Login";


import NotFoundComponent from "../../components/pages/404"
import LogoutComponent from "../../components/pages/account/Logout";
import HelloWorld from "../../components/HelloWorld";



const routes = [
  /*{
    path: '/',
    name: 'home',
    component: HelloWorld
  },*/
 /* {
    path: '/about',
    name: 'about',
    component: About
  },*/
  {
    path: '/',
    name: 'home',
    component: HelloWorld
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: LogoutComponent,
    beforeEnter: guards.isLoggedIn,
  },
  {
    path: '/document',
    component: DocumentApp,
    beforeEnter: guards.isLoggedIn,
    children: routes_documents
  },
  {
    path:'*',
    component: NotFoundComponent
  }
]

export default routes
