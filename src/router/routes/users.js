import UsersList from "../../components/pages/Users/UsersList";
import Users from "../../components/pages/Users";


const routes = [
  {
    path: '/',
    name: 'list',
    component: UsersList
  },
  {
    path: '/me',
    name: 'me',
    component: Users
  },
]

export default routes
