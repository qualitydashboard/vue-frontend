import About from "../../components/About";
import HelloWorld from "../../components/HelloWorld";
import routes_users from './users'
import UsersApp from "../../components/pages/Users/UsersApp";
import guards from "../guards"
import Login from "../../components/pages/Login";


const routes = [
  {
    path: '/',
    name: 'home',
    component: HelloWorld
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/users',
    component: UsersApp,
    beforeEnter: guards.isLoggedIn,
    children: routes_users
  }
]

export default routes
