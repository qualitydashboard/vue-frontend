import AuthService from './auth.service'

export default function getAuthHeader() {
  return new Promise((resolve, reject) => {
    AuthService.refreshAccessIfRequired()
        .then(token=>{
          resolve({ Authorization: 'Bearer ' + token })
        })
        .catch(err=>{
          reject(err)
        });
  });
}
