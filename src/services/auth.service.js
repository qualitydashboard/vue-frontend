import axios from 'axios';
import User from "../models/user";

//const API_URL = 'https://qualitydashboard-staging.herokuapp.com/api'//process.env.API_URL + '/api';
const API_URL = 'http://localhost:8000' + '/api';


class AuthService {
  login (user){
    return axios
      .post(API_URL + '/token/', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.access) {
          console.log(response)
          user.setToken(response.data);
          user.save()
        }

        return user;
      });
  }

  refreshAccessIfRequired(){
    return new Promise((resolve, reject) => {
      const user = User.getCurrent();
      if (user!=null) {
        if (user.hasAccessExpired()) {
          axios
              .post(`${API_URL}/token/refresh/`,{
                refresh: user.token.refresh
              })
              .then(response=>{
                 if (response.data.access) {
                      console.log(response);
                      user.token.access = response.data.access;
                      console.log(user)
                      user.save();
                      resolve(user.token.access)
                  }else{
                    reject('can not refresh token')
                  }
              })

        }else{
          resolve(user.getJWT())
        }
      }else {
        reject('no user')
      }
    })

  }


  logout (){
    localStorage.removeItem('user');
  }

  register (user){
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();

