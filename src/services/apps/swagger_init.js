var Swagger = require('swagger-client');
import authHeader from '../auth-header';
import API_URL from '../../../config/prod.env'

var SWAGGER_URL = `${API_URL}/swagger.json`;

/*
    Why using swagger-client ?
    + Automated route discovery
        -> related to spec_action_id instead of fixed route

    + parameters validation with swagger ?
        -> first checking on client side

    + fetched definition
        - dynamic update of form fields



    - need to reload swagger.json before each request
        -> cache specs ?

 */

// With Promise
var client = new Swagger({
    url: SWAGGER_URL
    , usePromise: true
    , authorizations: {
        accessTokenAuth: new Swagger.ApiKeyAuthorization('Authorization', authHeader().Authorization, 'header')
    }
});

client.then(function(api) {
   console.log(api)
})



module.exports = {
    client: client
}
