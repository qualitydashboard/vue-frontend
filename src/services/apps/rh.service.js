import axios from 'axios';
import getAuthHeader from '../auth-header';
import config from '../../../config/dev.env'
const API_URL_RH = `${config.API_URL}/RH`;

/*
 * RH endpoints
 *  ------------------------
 *   shall be replaced by swagger client -- f relevant --
 */
class RhService {
    getMemberList () {
        return getAuthHeader()
            .then(authHeader => {
                return axios.get(API_URL_RH + '/member.json', {headers: authHeader});
            });
    }

    getMember (pk=-1){
        // if pk=null, return currentUserMember
        return getAuthHeader()
            .then(authHeader=>{
                return axios.get(`${API_URL_RH}/member/${pk}.json`,{headers: authHeader});
            });
    }


    createMember(member){
        return getAuthHeader()
            .then(authHeader=>{
                return axios.post(`${API_URL_RH}/member/`, member, {headers: authHeader});
            })
    }
}

export default new RhService();
