import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `${process.env.API_URL}/api`

/*
 * Users endpoints
 *  ------------------------
 *   shall be replaced by swagger client
 */
class UserService {
  getList (){
    return axios.get(API_URL + '/users.json');
  }

  getUserBoard (){
    return axios.get(API_URL + '/users', { headers: authHeader() });
  }

}

export default new UserService();
